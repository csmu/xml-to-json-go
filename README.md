# xml-to-json

## Example usage

```
 ./xml-to-json -xml {path-to-valid-xml-file} -json {valid-path}
 ```

## Credits

Thanks to

https://github.com/Jeffail/gabs

https://github.com/basgys/goxml2json