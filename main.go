package main

import (
	"bytes"
	"flag"
	"os"

	"github.com/Jeffail/gabs"
	xml2json "github.com/basgys/goxml2json"
)

func main() {

	var err error
	var inputXMLPath *string
	var inputXMLFile *os.File
	var outputJSONPath *string
	var outputJSONBytesBuffer *bytes.Buffer
	var outputJSONFile *os.File
	var outputJSONGabsContainer *gabs.Container

	inputXMLPath = flag.String("xml", "", "A valid path to a valid xml input file.")
	outputJSONPath = flag.String("json", "", "A valid path for the output json file.")

	flag.Parse()

	if *inputXMLPath == "" || *outputJSONPath == "" {
		flag.PrintDefaults()
	} else {
		inputXMLFile, err = os.Open(*inputXMLPath)
		if err != nil {
			panic(err)
		} else {
			defer inputXMLFile.Close()
			outputJSONFile, err = os.Create(*outputJSONPath)
			if err != nil {
				panic(err)
			} else {
				defer outputJSONFile.Close()
				outputJSONBytesBuffer, err = xml2json.Convert(inputXMLFile)
				if err != nil {
					panic(err)
				} else {
					outputJSONGabsContainer, err = gabs.ParseJSON(outputJSONBytesBuffer.Bytes())
					if err != nil {
						panic(err)
					} else {
						outputJSONFile.WriteString(outputJSONGabsContainer.StringIndent("", "    "))
					}
				}
			}
		}
	}
}
